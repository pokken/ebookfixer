# Takes an ebook directory and fixes all the ebooks in the directory.
#

# Use cases
# 1. Pagenumbers
# 2. a number it finds at the end of a line; note this could theoretically
#    get a number in the text I guess, probably need to look for something
#    else to verify, but for my ebooks it seems to work

# external projects
import ebooklib
from ebooklib import epub
from bs4 import BeautifulSoup
import argparse

# python internals
import pathlib
import sys
import logging


# project imports

# setup
FORMAT = '%(asctime)-15s|%(module)s|%(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


# Functions!
def save_ebook(ebook, fp): 
    '''Save a parsed xml doc to its new location'''
    fn_out = fp.stem + "__edited" + fp.suffix
    fp_out = fp.parent / fn_out

    epub.write_epub(fp_out, ebook, {})

def clean_html(content): 
    '''Main function that cleans the html of each chapter, takes raw string'''

    #print(f"Working with raw content {content}")
    chapter = BeautifulSoup(content, "lxml")

    body_out = "<body>" # the body string 
    # note: it might be a good idea to use the find_all with
    # the text = regex option 

    for element in chapter.html.body.find_all('p'):
        for parser in parsers:
            element = parser(element)
            logger.debug(f"Appending {element}")
        body_out += str(element)
    
    body_out += "</body>"

    # replace the body with our new body text that's been parsed    
    #chapter.body = chapter_html.new_tag(body_out)
    chapter.html.body.extract()
    #chapter.html.append(chapter.new_tag(body_out))
    chapter.html.append(BeautifulSoup(body_out, "lxml"))


    logger.debug("---------------------------------")
    logger.debug("Returning new chapter: \n")
    logger.debug(chapter)
    logger.debug("---------------------------------")
    
    return str(chapter)


# Cleaners take an HTML element, fix it, and return 
def cleaner_pagenumbers(element):
    '''Cleans pagenumbers that exist on newlines in separate paragraph tags'''
    #print(f"parsing {element} for pagenumbers")

    try:
        #print(f"Element text is {element.text}")
        int(element.text)
        # Return a blank string!
        logger.debug(f"Found a chapter marking in {element}, returning empty.")
        element = ""
        return element
    except Exception as e:
        # 
        logger.debug(e)


    return element

def cleaner_early_pagenumbers(element):
    '''Cleans page numbers that occur at the end of a paragraph
    extraneously'''

    # Find the last number by splitting by spaces
    # Note: this parser has a side effect that it will remove a loose number
    # from the end of a paragraph tag. This *should not* happen in normal writing
    # because a number at the end of a line should be terminated in a period, e.g.
    # '172.' which will not convert to an integer. 
    # If your ebook has bad formatting it's possible you could lose actual meaningful
    # text with this.
    if element:
        words = element.text.split(' ')
        try:
            int(words[-1])
            logger.debug(f"Found extraneous chapter number in {element} is {words[-1]}. Removing!")
            element.string = " ".join(words[0:-2])
            logger.debug(f"New text is {element.text}")
        except ValueError:
            pass
        except Exception:
            logger.exception("Found exception")

    return element

parsers = [
    cleaner_pagenumbers,
    cleaner_early_pagenumbers
]



if __name__ == "__main__":

    # basic argparse stuff for taking a file path to work from, is recursive, saves to the parallel loc
    # of a given epub
    parser = argparse.ArgumentParser(description="Take a command line argument for the directory to scan for ebooks.")

    parser.add_argument('--ebookpath',
        help="Fully qualified path to your ebooks, in quotes if there are spaces!",
        type=pathlib.Path,
        required=True
    
    )
    args = parser.parse_args()

    #ebook_folder = pathlib.Path(r"C:\Users\rpsan\Calibre Library")
    ebook_folder = args.ebookpath
    
    for f in ebook_folder.rglob("*.epub"):

        # skip books that we've already edited
        if "__edited" in f.name: 
            continue

        book = epub.read_epub(f)
        chapters = book.get_items_of_type(ebooklib.ITEM_DOCUMENT)

        for chapter in chapters:
            parsed_content = clean_html(chapter.content)
            chapter.content = parsed_content.encode('UTF-8')
            #print(chapter.content)



        save_ebook(book, f)

            
            
