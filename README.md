# Usage
python ebookfixer.py --ebookpath "C:\Users\<user>\Calibre Library"

This runs two parsers on each document chapter of an ebook.
* clean page numbers - extraneous page numbers in isolated paragraph tags should be removed. 
These tags look like:
This is some text.

34

And this is some more text.

The 34 paragraph tag will be deleted. We don't need page numbers in epubs. 

* clean page numbers embedded in text - sometimes an epub conversion leaves page numbers
running free in text, e.g. "And then he ate a sandwich. 276" The parser gets rid of the last
word in any paragraph if it can be converted to an integer. It will *NOT* remove "And then he
ate a sandwich 276." because "276." does not convert to an integer, so properly formatted
content should not be lost.

Possible side effects
1. Images might be stripped out. I tried to avoid this by only feeding in document chapters into the parsers
   but an image tag that is not wrapped in a paragraph tag could be stripped. 

2. Numbers at the end of a paragraph that are not terminated by a period will be stripped.

3. The ebooklib library messes up title pages because it does not properly save headers. 

Because the original book is preserved you 



# Some notes!


Bad formatting we want to remove (stupid page numbers)


* with the number in a separate line as expected

<p class="calibre1">5</p>\n<p class="calibre1"><a id="p6"></a>

* with the number inside the original P tag


7</p>\n<p class="calibre1"><a id="p8"></a>





# mucking with the html

# first item
content = book.items[6].content
#print(content)

parsed_html = ebooklib.utils.parse_html_string(content)
#print(dir(parsed_html))

for html in parsed_html:
    for element in html:
        print(f"Element tag: {element.tag} -- Content: {element.text}")


# old header stripping code

    #print(f"Assigning the header of {html.head}")
    # Because of weird ebook stuff we have to preserve the header exactly as it was

    #content_header = content[0:content.index(r'</head>')]
    content = content.decode('UTF-8')
    # Strip the header out
    #print(f"Index of head end tag is {content[0: content.find('</head>') + 7]}")
    header = content[0: content.find('</head>') + 7]



# Why do the HTML headers get stripped?

There is some kinda bug in the save code that strips all the headers out whether I modify the html or not. I guess it really does not matter honestly since it's just the title page that gets messed up.

# TODO
write the parsers


p_tags = element.find_all('p', {"class": 'calibre1'})

print(p_tags)

finding tags with regex on text in bs4
https://stackoverflow.com/questions/9007653/how-to-find-tag-with-particular-text-with-beautiful-soup


